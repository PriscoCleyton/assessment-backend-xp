<?php
ob_start();

require __DIR__ . "/vendor/autoload.php";

/**
 * BOOTSTRAP
 */

use CoffeeCode\Router\Router;
use Source\Core\Session;

$session = new Session();
$route = new Router(url(), ":");

/**
 * ROUTES
 */
$route->namespace("Source\App\Admin");
$route->group(null);
$route->get("/", "DashboardController:index");


$route->group('/dashboard');
$route->get("/", "DashboardController:index");

$route->group('/category');
$route->get("/", "CategoryController:index");
$route->get("/new", "CategoryController:new");
$route->get("/edit/{id}", "CategoryController:edit");
$route->post("/create", "CategoryController:create");
$route->post("/update/{id}", "CategoryController:update");
$route->get("/delete/{id}", "CategoryController:delete");


$route->group('/product');
$route->get("/", "ProductController:index");
$route->get("/new", "ProductController:new");
$route->get("/edit/{id}", "ProductController:edit");
$route->post("/create", "ProductController:create");
$route->post("/update/{id}", "ProductController:update");
$route->get("/delete/{id}", "ProductController:delete");



//END ADMIN
$route->namespace("Source\App");

/**
 * ERROR ROUTES
 */
$route->group("/ops");
$route->get("/{errcode}", "Web:error");

/**
 * ROUTE
 */
$route->dispatch();

/**
 * ERROR REDIRECT
 */
if ($route->error()) {
    $route->redirect("/ops/{$route->error()}");
}

ob_end_flush();