
<?php $v->insert("views/_header.php"); ?>
<body>
  <!-- Main Content -->
  <?php 
    if (isset($data)) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
  ?>
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="<?= url("/category/new"); ?>" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php if (isset($categories)):?>
      <?php foreach ($categories as $key => $category):?> 
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $category->name ?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $category->code ?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><a href="<?= url("/category/edit/{$category->id}"); ?>"> <span>Edit</span></a></div>
            <div class="action delete" > 
              <?php 
              $action = url("/category/delete/{$category->id}");
              ?>
              <a href=# onclick="return confirmacao('<?= $action ?>')"><span><?= "Delete" ?></span></a>
            </div>
          </div>
        </td>
      </tr>
      <?php endforeach ?>
      <?php else: ?>
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content">Não há registros</span>
          </td>
        </tr>
      <?php endif ?> 
    </table>
  </main>
  <!-- Main Content -->
  <script language=javascript>
    function confirmacao($action){
      if (confirm("Deseja excluir o registro?")) {     
          // alert($action);
          window.location.assign($action)
      }
    }
    
  </script>
<?php $v->insert("views/_footer.php"); ?>
