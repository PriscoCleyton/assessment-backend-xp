<?php $v->insert("views/_header.php"); ?>
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Edit Category</h1>
    
    
    <form action="<?= url("/category/update/{$category->id}"); ?>" method="post">
      <input type="hidden" name="action" value="update"/>
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input name="name" type="text" id="category-name" class="input-text" value="<?= $category->name; ?>"/>
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input name="code" type="text" id="category-code" class="input-text" value="<?= $category->code; ?>" />
        
      </div>
      <div class="actions-form">
      <a href="<?= url("/category"); ?>" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Update" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
  <?php $v->insert("views/_footer.php"); ?>