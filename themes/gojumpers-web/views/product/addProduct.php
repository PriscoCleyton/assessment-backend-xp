<?php $v->insert("views/_header.php"); ?>

<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form action="<?= url("/product/create"); ?>" method="post" enctype="multipart/form-data">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input name="sku" type="text" id="sku" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input name="name"  type="text" id="name" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input name="price"  type="text" id="price" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input name="quantity"  type="text" id="quantity" class="input-text" /> 
      </div>
      <div class="input-field">
      <label for="category" class="label">Categories</label>  
      <?php foreach ($categories as $key => $category):?>
        
        <!-- <select multiple id="category" class="input-text">
          <option>Category 1</option>
          <option>Category 2</option>
          <option>Category 3</option>
          <option>Category 4</option>
        </select> -->
        <input type="checkbox" name="categories[]" value="<?= $category->id ?>"> <?= $category->name ?>
        <?php endforeach ?>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea name="description" id="description" class="input-text"></textarea>
      </div>
      <div class="input-field">
        <label for="description" class="label">Imagem do Produto:</label>
        <input data-image=".j_profile_image" type="file" class="input-text"  name="photo"/>
      </div>  
      <div class="actions-form">
        <a href="<?= url("/product"); ?>" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<?php $v->insert("views/_footer.php"); ?>