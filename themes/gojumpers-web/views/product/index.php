<?php $v->insert("views/_header.php"); ?>

<body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="<?= url("/product/new"); ?>" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php if (isset($products)):?>
      <?php foreach ($products as $key => $product):?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$product->name?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$product->sku_code?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$product->price?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$product->qtd?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">            
            <?php 
              $pcs = $product->productCategory();
              if (is_array($pcs) || is_object($pcs)){
                  foreach ($pcs as $key => $pc) {
                      echo $pc->category()->name." | ";
                  }
              }                        
            ?>
          </span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><a href="<?= url("/product/edit/{$product->id}"); ?>"><span>Edit</span></a></div>
              <?php 
                $action = url("/product/delete/{$product->id}");
              ?>
              <a href=# onclick="return confirmacao('<?= $action ?>')"><span>Delete</span></a>            
          </div>
        </td>
      </tr>
      <?php endforeach ?>
      <?php else: ?>
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content">Não há registros</span>
          </td>
        </tr>
      <?php endif ?> 
    </table>
  </main>
  <!-- Main Content -->
  <script language=javascript>
    function confirmacao($action){
      if (confirm("Deseja excluir o registro?")) {     
          // alert($action);
          window.location.assign($action)
      }
    }
    
  </script>
  <!-- Footer -->
<?php $v->insert("views/_footer.php"); ?>