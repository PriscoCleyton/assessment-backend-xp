<?php

namespace Source\Models;

use Source\Core\Model;


class AppProductCategory extends Model
{
    
    public function __construct()
    {
        parent::__construct("tbl_product_category", ["id"], ["product_id", "category_id"]);
    }

    public function category(): ?AppCategory
    {
        if ($this->category_id) {
            return (new AppCategory())->findById($this->category_id);
        }
        return null;
    }



    
   

  
}