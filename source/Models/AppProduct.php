<?php

namespace Source\Models;

use Source\Core\Model;

class AppProduct extends Model
{
    
    public function __construct()
    {
        parent::__construct("tbl_products", ["id"], ["name", "sku_code"]);
    }

    public function productCategory(){
        if ($this->id) {
            return (new AppProductCategory())->find("product_id = :product_id", "product_id={$this->id}")->fetch(true);
        }
        return null;
        
    }

}