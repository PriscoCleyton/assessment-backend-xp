<?php

namespace Source\Models;

use Source\Core\Model;

class AppCategory extends Model
{
    
    public function __construct()
    {
        parent::__construct("tbl_categories", ["id"], ["name", "code"]);
    }

    
}