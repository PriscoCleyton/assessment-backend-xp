<?php
/**
 * DATABASE
 */
define("CONF_DB_HOST", "localhost");
define("CONF_DB_USER", "root");
define("CONF_DB_PASS", "");
define("CONF_DB_NAME", "gojumpers");

/**
 * PROJECT URLs
 */
define("CONF_URL_BASE", "http://localhost/assessment-backend-xp");
define("CONF_URL_TEST", "");

/**
 * SITE
 */
define("CONF_SITE_NAME", "GoJumpers");
define("CONF_SITE_LANG", "pt_BR");
define("CONF_SITE_DOMAIN", "");


/**
 * DATES
 */
define("CONF_DATE_BR", "d/m/Y H:i:s");
define("CONF_DATE_APP", "Y-m-d H:i:s");



/**
 * VIEW
 */
define("CONF_VIEW_PATH", __DIR__ . "/../../shared/views");
define("CONF_VIEW_EXT", "php");
define("CONF_VIEW_ADMIN", "gojumpers-web");

/**
 * UPLOAD
 */
define("CONF_UPLOAD_DIR", "uploads");
define("CONF_UPLOAD_IMAGE_DIR", "images");
define("CONF_UPLOAD_FILE_DIR", "files");
define("CONF_UPLOAD_MEDIA_DIR", "medias");

/**
 * IMAGES
 */
define("CONF_IMAGE_CACHE", CONF_UPLOAD_DIR . "/" . CONF_UPLOAD_IMAGE_DIR . "/cache");
define("CONF_IMAGE_SIZE", 5000);
define("CONF_IMAGE_QUALITY", ["jpg" => 75, "png" => 5,"jpeg" => 75]);


