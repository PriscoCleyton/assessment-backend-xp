<?php

namespace Source\App\Admin;

use Source\Models\AppProduct;

/**
 * Class DashboardController
 * @package Source\App\Admin
 */
class DashboardController extends Admin
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function index(?array $data): void
    {
        $products = (new AppProduct())->find()->fetch(true);
        $qtdProducts = (new AppProduct())->find()->count();
        echo $this->view->render("views/dashboard/index", [            
            "products" => $products,           
            "qtdProducts" => $qtdProducts,   
        ]);
    }

    

    
}