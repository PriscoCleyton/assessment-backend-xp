<?php

namespace Source\App\Admin;
use Source\Models\AppCategory;


/**
 * Class CategoryController
 * @package Source\App\Admin
 */
class CategoryController extends Admin
{
    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function index(?array $data): void
    {
        $categories = (new AppCategory())->find()->fetch(true);
        
        echo $this->view->render("views/category/index", [            
            "categories" => $categories,           
        ]);
    }

    

    public function new()
    {
        echo $this->view->render("views/category/addCategory", []);        
    }


    public function create(?array $data): void
    {
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

        $AppCategory = new AppCategory();
        $AppCategory->name = $data["name"];
        $AppCategory->code = $data["code"];
        
        if (!$AppCategory->save()) {
            $json["message"] = "Não foi possível inserir o registro";
            echo json_encode($json);
            return;
        }
        redirect("/category");
    }

    public function edit(?array $data): void
    {
        $category = null;
        if (!empty($data["id"])) {
            $categoryId = filter_var($data["id"], FILTER_VALIDATE_INT);
            $category = (new AppCategory())->findById($categoryId);
            echo $this->view->render("views/category/editCategory", [
                "category" => $category,
            ]);
            return;
        }
    }

    public function update(?array $data): void
    {
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
        $AppCategory = (new AppCategory())->findById($data["id"]);

        $AppCategory->name = $data["name"];
        $AppCategory->code = $data["code"];
      

        if (!$AppCategory->save()) {
            $json["message"] = $AppCategory->message()->render();
            echo json_encode($json);
            return;
        }

        redirect("/category");
    }

    public function delete(?array $data): void
    {        
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
        $AppCategory = (new AppCategory())->findById($data["id"]);
        $AppCategory->destroy();
        redirect("/category");
        
    }
}