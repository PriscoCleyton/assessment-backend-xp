<?php

namespace Source\App\Admin;
use Source\Models\AppProduct;
use Source\Models\AppCategory;
use Source\Models\AppProductCategory;
use Source\Support\Thumb;
use Source\Support\Upload;
/**
 * Class ProductController
 * @package Source\App\Admin
 */
class ProductController extends Admin
{
    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function index(?array $data): void
    {
        $products = (new AppProduct())
                    ->find()
                    ->fetch(true);
        
        // foreach ($products as $i => $product) {
        //     $pcs = (new AppProductCategory())->find("product_id = :product_id", "product_id={$product->id}")->fetch(true);
        //     if (is_array($pcs) || is_object($pcs)){
        //         foreach ($pcs as $key => $pc) {
        //             $ram[] = $pc->category()->name;
        //         }
        //     }                        
        // }
        echo $this->view->render("views/product/index", [            
            "products" => $products,              
        ]);
    }

    

    public function new()
    {
        $categories = (new AppCategory())->find()->fetch(true);       
        echo $this->view->render("views/product/addProduct", [
            "categories" => $categories
        ]);        
    }
  

    public function create(?array $data): void
    {
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);            
        $AppProduct = new AppProduct();
        $AppProductCategory = new AppProductCategory();
        
        $AppProduct->sku_code       = $data["sku"];
        $AppProduct->name           = $data["name"];        
        $AppProduct->price          = $data["price"];
        $AppProduct->description    = $data["description"];
        $AppProduct->qtd            = $data["quantity"];    
        $AppProduct->photo          = '';
        
        if (!empty($_FILES["photo"])) {
            $file = $_FILES["photo"];
            $upload = new Upload();
            
            (new Thumb())->flush("uploads/{$AppProduct->name}");
            $upload->remove("uploads/{$AppProduct->name}");
                        
            if (!$AppProduct->photo = $upload->image($file, "{$AppProduct->name}-" . time(), 360)) {
                $json["message"] = "Erro ao tentar fazer upload da imagem.";
                echo json_encode($json);
                return;
            }
            
        }

        
        if (!$AppProduct->save()) {
            $json["message"] = "Não foi possível inserir o registro";
            echo json_encode($json);
            return;
        }
        
        foreach ($data['categories'] as $category_id) {
            $AppProductCategory->id = null;
            $AppProductCategory->category_id = $category_id;
            $AppProductCategory->product_id  = $AppProduct->id;
            $AppProductCategory->save();            
        }
        redirect("/product");
    }

    public function edit(?array $data): void
    {    
        if (!empty($data["id"])) {
            $productId = filter_var($data["id"], FILTER_VALIDATE_INT);
            $product = (new AppProduct())->findById($productId);
            $categories = (new AppCategory())->find()->fetch(true);
            echo $this->view->render("views/product/editProduct", [
                "product" => $product,
                "categories" => $categories,
            ]);
            return;
        }
    }

    public function update(?array $data): void
    {
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
        $AppProduct = (new AppProduct())->findById($data["id"]);
        
        $AppProduct->sku_code       = $data["sku"];
        $AppProduct->name           = $data["name"];        
        $AppProduct->price          = $data["price"];
        $AppProduct->description    = $data["description"];
        $AppProduct->qtd            = $data["quantity"];          
    
        
        if (!empty($_FILES["photo"])) {
            $file = $_FILES["photo"];
            $upload = new Upload();
            
            (new Thumb())->flush("uploads/{$AppProduct->name}");
            $upload->remove("uploads/{$AppProduct->name}");
                        
            if (!$AppProduct->photo = $upload->image($file, "{$AppProduct->name}-" . time(), 360)) {
                $json["message"] = "Erro ao tentar fazer upload da imagem.";
                echo json_encode($json);
                return;
            }            
        }
        if (!$AppProduct->save()) {
            $json["message"] = $AppProduct->message()->render();
            echo json_encode($json);
            return;
        }

        (new AppProductCategory())->delete("product_id = :product_id", "product_id={$AppProduct->id}");
        $AppProductCategory = new AppProductCategory();
        foreach ($data['categories'] as $category_id) {
            $AppProductCategory->id = null;
            $AppProductCategory->category_id = $category_id;
            $AppProductCategory->product_id  = $AppProduct->id;
            $AppProductCategory->save();            
        }

        redirect("/product");
    }

    public function delete(?array $data): void
    {                
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);        
        $AppProduct = (new AppProduct())->findById($data["id"]);       
        $AppProductCategory = (new AppProductCategory())
                                ->delete("product_id = :product_id", "product_id={$AppProduct->id}"
                            );
        $AppProduct->destroy();
        redirect("/product");   
        
    }
}