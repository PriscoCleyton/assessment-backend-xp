-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.18-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para gojumpers
CREATE DATABASE IF NOT EXISTS `gojumpers` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `gojumpers`;

-- Copiando estrutura para tabela gojumpers.tbl_categories
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela gojumpers.tbl_categories: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_categories` DISABLE KEYS */;
INSERT INTO `tbl_categories` (`id`, `name`, `code`) VALUES
	(4, 'Sport', 'S001'),
	(6, 'Casual', 'C001'),
	(12, 'Frágeis', 'F002');
/*!40000 ALTER TABLE `tbl_categories` ENABLE KEYS */;

-- Copiando estrutura para tabela gojumpers.tbl_products
CREATE TABLE IF NOT EXISTS `tbl_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `sku_code` varchar(50) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela gojumpers.tbl_products: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_products` DISABLE KEYS */;
INSERT INTO `tbl_products` (`id`, `name`, `sku_code`, `price`, `description`, `qtd`, `photo`) VALUES
	(13, 'Produto 13', 'dsa 13', 13.13, 'ccx', 13, 'images/2021/09/produto-13-1632882362.png'),
	(14, 'Produto 5', 'dsa 4', 1.00, '223', 1, 'images/2021/09/produto-5-1632882179.jpg'),
	(15, 'Produto 12', 'Cod 12', 120.00, 'Product 12 new', 2, 'images/2021/09/produto-12-1632882395.jpg'),
	(17, 'Produto novo', 'new code sku 001', 12.12, 'Produto teste novo', 12, 'images/2021/09/produto-novo-1632882195.jpg'),
	(18, 'Produto 5', '55-555', 5.10, 'Produto teste 5', 555, 'images/2021/09/produto-5-1632882412.png');
/*!40000 ALTER TABLE `tbl_products` ENABLE KEYS */;

-- Copiando estrutura para tabela gojumpers.tbl_product_category
CREATE TABLE IF NOT EXISTS `tbl_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `tbl_product_category_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`id`),
  CONSTRAINT `tbl_product_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela gojumpers.tbl_product_category: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_product_category` DISABLE KEYS */;
INSERT INTO `tbl_product_category` (`id`, `product_id`, `category_id`) VALUES
	(31, 14, 4),
	(32, 14, 6),
	(33, 14, 12),
	(36, 17, 4),
	(37, 17, 12),
	(40, 13, 4),
	(41, 13, 12),
	(42, 15, 4),
	(43, 15, 6),
	(44, 18, 12);
/*!40000 ALTER TABLE `tbl_product_category` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
