# Webjump | Prisco Cleyton - Vaga Desenvolvedor Back-end
### Pré-requisitos
    - PHP >= 7.1
    - MySQL
    - Composer
### Sobre
Mini Sistema de gerenciamento de produtos, com dashboard, formulário de cadastro de produtos e categorias. Com possibilidade de colocar imagem no produto.

### Instalação
-   Clone este repositório
-   Acesse a pasta do projeto no terminal/cmd e rode: ```composer install```
-   Execute o dumb ``database.sql`` que está na raiz do projeto para criar o banco de dados e suas tabelas.   
-   Configue o arquivo ``Config.php`` que se encontra no diretório ``source\Boot\`` com as informações do seu banco de dados e o CONF_URL_BASE com a url do projeto (ex: http://localhost/gojumper).


  
### Bibliotecas/Dependências
Utilizei alguns dependências para me ajudar nas rotas, otimizaçao de códigos, facilitar upload de imagem.
```
    "php": "^7.1",
    "ext-pdo": "^7.1",
    "league/plates": "v4.0.0-alpha",
    "matthiasmullie/minify": "^1.3",
    "coffeecode/uploader": "^1.0",
    "coffeecode/cropper": "^1.0",
    "coffeecode/router": "^1.0"
```